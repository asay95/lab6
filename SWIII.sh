#!/bin/bash

## Lab 6 Webscraper
## For my webscraper assignment, I will grab the movie script for
## Star Wars: Episode III - Revenge of the Sith and change it a bit.
## I will change the main characters names to cartoon character names
## who happen to be from under the sea.

## Use sed to replace names. Be careful of case sensitive names.
## Use sed to remove all the clutter that comes from
## grabbing the webpage.
## Then send the edited file into a new text file so it can be readable.

curl -s https://imsdb.com:443/scripts/Star-Wars-Revenge-of-the-Sith.html \
| sed 's/[oO][bB][iI]-[wW][aA][nN]/Patrick/g' \
| sed 's/[aA][nN][aA][kK][iI][nN]/SpongeBob/g' \
| sed 's/[yY][oO][dD][aA]/Mr. Krabs/g' \
| sed 's/[pP][aA][dD][mM][eE]/Sandy/g' \
| sed 's/[pP][aA][lL][pP][aA][tT][iI][nN][eE]/Plankton/g' \
| sed 's/[kK][eE][nN][oO][bB][iI]/Star/g' \
| sed 's/[vV][aA][dD][eE][rR]/SquarePants/g' \
| sed 's/[sS][ilI][dD][iI][oO][uU][sS]/Chum/g' \
| sed 's/[mM][aA][cC][eE]/Squidward/g' \
| sed 's/[gG][rR][iI][eE][vV][oO][uU][sS]/Gary/g' \
| sed 's/[dD][oO][oO][kK][uU]/Hasselhoff/g' \
| sed 's/[sS][kK][yY][wW][aA][lL][kK][eE][rR]/Pants/g' \
| sed 's/<.*>//' \
| sed -n '/STAR WARS EPISODE 3/,$p' \
| sed -n '/Back to IMSDb/q;p' >> SpongeWarsRevengeOfTheChum.txt
